Downloader for Packt free e-book of the day

REWRITE OF: https://github.com/igustin/packt

{{{
Packt Free DL Default Configuration v0.03
http://gitlab.com/krayon/packtfreedl/


Packt Free DL Default Configuration downloads the latest free book from Packt Publishing.

Usage: packtfreedl.bash -h|--help
       packtfreedl.bash -V|--version
       packtfreedl.bash -C|--configuration
       packtfreedl.bash [-v|--verbose]

-h|--help           - Displays this help
-V|--version        - Displays the program version
-C|--configuration  - Outputs the default configuration that can be placed in
                          /etc/packtfreedl.conf
                      or
                          /home/krayon/.packtfreedlrc
                      for editing.
-v|--verbose        - Displays extra debugging information.  This is the same
                      as setting DEBUG=1 in your config.
Example: packtfreedl.bash
}}}

== FEATURES ==

* Claims and (optionally) downloads free book of the day from Packt Publishing.
* **[NEW]** Will resume downloads of files if run again.
* **[NEW]** Can download the associated code as well as the book title.
* Seperate timeout variable for downloads (DLTIMEOUT).
* **[NEW]** ANTI-FEATURE: Inability to tell if a download failed to continue or
  is just completed already due to curl bug 1163
  ( https://github.com/curl/curl/issues/1163 )

== CONFIGURATION ==

To generate the default configuration for modification, run:
{{{
./packtfreedl.bash -C >~/.packtfreedlrc
}}}

Now, you can edit ~/.packtfreedlrc in your favourite editor.

The configuration file will look something like this:
{{{
# Packt Free DL Default Configuration
# ===================================

# DEBUG
#   This defines debug mode which will output verbose info to stderr
#   or, if configured, the debug file ( ERROR_LOG ).
DEBUG=0

# ERROR_LOG
#   The file to output errors and debug statements (when DEBUG !=
#   0) instead of stderr.
#ERROR_LOG="/tmp/packtfreedl.log"

# DOWNLOAD_DIR
#   The directory to download the ebooks to
DOWNLOAD_DIR="${HOME}/Downloads/"

# CLAIM_EBOOKS
#   If packtfreedl should log in and try to claim the ebooks. NOTE: If this is
#   false (0), ebooks will not be downloaded (USER_ID, PASSWORD and
#   DOWNLOAD_FORMATS is ignored).
CLAIM_EBOOKS=1

# DOWNLOAD_FORMATS
#   An array of formats you want to download. Uses the format:
#     DOWNLOAD_FORMATS=(format1 format2 format3)
#   Obviously only formats that are available for download are valid. Currently
#   Packt appears to offer pdf, mobi and epub. You can also specify the special
#   format 'code' if you want to download the associated code for the book.
DOWNLOAD_FORMATS=(
    'epub'
    'pdf'
    'mobi'
)

# USER_ID
#   Your user ID for the Packt Publishing website. This is most
#   likely the email address you used to register on their
#   website.
USER_ID="your.email@example.com"

# PASSWORD
#   Your password for the Packt Publishing website.
PASSWORD="your.password.here"

# TIME_SLEEP
#   The amount of time (in seconds) to sleep between requests
TIME_SLEEP=1

# TIMEOUT
#   The amount of time (in seconds) to wait for an entire web transaction
TIMEOUT=30

# DLTIMEOUT
#   The amount of time (in seconds) to wait for downloading of files. Keep in
#   mind that if you're downloading code, this may need to be set considerably
#   large. If a timeout does occur, packtfreedl will try to resume the download
#   when run again.
DLTIMEOUT=600

# RETRIES
#   The number of retries before giving up
RETRIES=5

# USER_AGENT
#   The user agent to use for downloading
USER_AGENT="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:51.0) Gecko/20100101 Firefox/51.0"
}}}

== EXAMPLE ==

Example of the script in use:
{{{
$ ./packtfreedl.bash
Logging in...
Looking up free book...
Free book: Learning C# by Developing Games with Unity 5.x - Second Edition...
Claiming book...
Download format: epub...
Download format: pdf...
Download format: code...
}}}

== BUGS ==

* Due to a curl bug ( https://github.com/curl/curl/issues/1163 ), packtfreedl is
  currently unable to tell the difference between an error downloading, or a
  completed file, trying to download again.

<<< vim:set ts=4 sw=4 tw=80 et ai si syn=creole: >>>
